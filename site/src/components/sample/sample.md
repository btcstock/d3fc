---
layout: section
section: core
title: Sample
namespace: Sample
---

The [d3fc-sample](https://github.com/d3fc/d3fc-sample) module contains a collection of components for down-sampling data using a variety of methods including mode-median, and largest triangle one (or three) bucket. For further details, see the project documentation.
